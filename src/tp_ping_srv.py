import argparse
import sys
import socket
import six
import time
import random

IP_ADRRES = "127.0.0.1"
BUFFER_SIZE = 1024
packets_dropped = 0


def proxy_ping(sock, count, remote_address, dest_address, myport):
    rtt = {}
    global packets_dropped
    print('ping proxy')
    operacion = 'd-' + str(count or 5)
    received = send_message(sock, operacion, dest_address, myport)
    if(received != b'start'):
        print('ERROR: Werent able to start the connction')
        sock.sendto(six.ensure_binary(str(rtt)), dest_address)
    else:
        for i in range(int(count)):
            init_time = get_time()
            message = 'PING ' + str(i)
            received = send_message(sock, message, dest_address, myport)
            rtt[str(i)] = round(get_time() - init_time, 2)
            received_size = len(received)
            print(str(received_size) + ' bytes from ' + str(dest_address[0])
                  + ':' + str(dest_address[1]) + ' seq =' + str(i) + ' time ='
                  + str(rtt[str(i)]) + ' ms')
    sock.sendto(six.ensure_binary(str(rtt) + str('-')
                + str(packets_dropped)), remote_address)


def reverse_ping(sock, count, remote_address, myport):
    rtt = {}
    global packets_dropped
    print('ping reverso')
    for i in range(int(count)):
        init_time = get_time()
        message = 'PING ' + str(i)
        received = send_message(sock, message, remote_address, myport)
        rtt[str(i)] = round(get_time() - init_time, 2)
        received_size = len(received)
        print(str(received_size) + ' bytes from ' + str(remote_address[0]) +
              ': seq =' + str(i) + ' time =' + str(rtt[str(i)]) + ' ms')
    sock.sendto(six.ensure_binary(str(rtt) + str('-') + str(packets_dropped)),
                remote_address)


def get_time():
    return time.time() * 1000


def wait_for_client(sock, port):
    global packets_dropped
    while True:
        try:
            message, _address = sock.recvfrom(port)
            return message
        except ConnectionRefusedError:
            packets_dropped = packets_dropped + 1
            return 'ERROR: Conection Refused'
        except Exception as e:
            packets_dropped = packets_dropped + 1
            return 'ERROR: ' + e


def send_message(sock, message, remote_address, myport):
    sock.sendto(six.ensure_binary(message), remote_address)
    return wait_for_client(sock, myport)


def direct_ping(sock, count='5', force=False):
    for _i in range(int(count)):
        message, remote_address = sock.recvfrom(BUFFER_SIZE)
        print('Recieve: ' + str(message))
        if force and random.randint(1, 6) == 6:
            time.sleep(5)
        sock.sendto(message, remote_address)
        print('Send: ' + str(message))


def parse_args(args=sys.argv[1:]):
    parser = argparse.ArgumentParser(description='Client Process.')
    parser.add_argument("-p", "--port", metavar='PORT',
                        type=int,
                        required=True,
                        help="server PORT address")
    return parser.parse_args()


def main():
    args = parse_args()
    address = (IP_ADRRES, args.port)
    print(address)
    while True:
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.bind(address)
        # recibe operacion
        msg, remote_address = sock.recvfrom(BUFFER_SIZE)
        if(six.ensure_str(msg).split('-')[0] == 'd'):
            sock.sendto(b'start', remote_address)
            loss_packs = 'force' in six.ensure_str(msg)
            direct_ping(sock, six.ensure_str(msg).split('-')[1], loss_packs)
        elif (six.ensure_str(msg).split('-')[0] == 'r'):
            sock.sendto(b'start', remote_address)
            reverse_ping(
                sock, six.ensure_str(msg).split('-')[1],
                remote_address, args.port)
        elif (six.ensure_str(msg).split('-')[0] == 'x'):
            sock.sendto(b'start', remote_address)
            count = six.ensure_str(msg).split('-')[1]
            dest = six.ensure_str(msg).split('-')[2]
            dest = (dest.split(':')[0], int(dest.split(':')[1]))
            proxy_ping(sock, count, remote_address, dest, args.port)
        else:
            print('Ping type not found.')

        sock.close()


if __name__ == "__main__":
    main()
