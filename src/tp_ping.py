import argparse
import sys
import socket
import time
import six
from builtins import round
from ast import literal_eval
IP_ADRRES = "127.0.0.1"
PORT = 8000
packets_dropped = 0
BUFFER_SIZE = 1024


def parse_args(args=sys.argv[1:]):
    parser = argparse.ArgumentParser(prog='Client Process.')
    group1 = parser.add_mutually_exclusive_group()
    group2 = parser.add_mutually_exclusive_group()

    group1.add_argument("-v", "--verbose", action="store_true",
                        help="increase output verbosity")
    group1.add_argument("-q", "--quiet", action="store_true",
                        help="decrease output verbosity")
    parser.add_argument("-s", "--server", metavar='ADDR',
                        help="server IP address", required=True)
    parser.add_argument("-c", "--count", type=int,
                        help="stop after < count > replies")
    parser.add_argument("-f", "--force", action="store_true",
                        help="force random package loss")
    group2.add_argument("-p", "--ping", action="store_true",
                        help="direct ping")
    group2.add_argument("-r", "--reverse", action="store_true",
                        help="reverse ping")
    group2.add_argument("-x", "--proxy", action="store_true",
                        help="proxy pin")
    parser.add_argument("-d", "--dest", metavar='ADDR',
                        help="destination IP address")

    return parser.parse_args()


def wait_for_server(sock, args):
    global packets_dropped
    while True:
        try:
            message, _address = sock.recvfrom(PORT)
            return message
        except ConnectionRefusedError:
            packets_dropped = packets_dropped + 1
            if not args.quiet:
                print('ERROR: Connection refused due to wrong adress')
            return 'ERROR: Conection Refused'
        except Exception as e:
            packets_dropped = packets_dropped + 1
            if not args.quiet:
                print('ERROR: ', str(e))
            return 'ERROR: ' + str(e)


def send_message(sock, message, args):
    sock.sendto(six.ensure_binary(message), args.server)
    return wait_for_server(sock, args)


def get_time():
    return time.time() * 1000


def direct_ping(sock, args):
    rtt = {}
    if not args.quiet:
        print('Operation : Direct Ping')
        print('Server Address : ', IP_ADRRES, ':', args.server)
        print('Client Address : ', IP_ADRRES)
    # envio de operacion
    operacion = 'd-' + str(args.count or 5)
    if args.force:
        operacion = operacion + '-force'
    if args.verbose:
        print('Inform server about direct ping sending message: ', operacion)
    received = send_message(sock, operacion, args)
    if args.verbose:
        print('Received: ', received)
    if(received != b'start'):
        print('ERROR: Werent able to start the connction')
        return rtt
    for i in range(args.count or 5):
        init_time = get_time()
        message = 'PING ' + str(i)
        if args.verbose:
            print('Sending message: ', message)
        received = send_message(sock, message, args)
        if args.verbose:
            print('Received message ', received)
        rtt[i] = round(get_time() - init_time, 2)
        received_size = len(received)
        print(str(received_size) + ' bytes from ' + IP_ADRRES +
              ': seq =' + str(i) + ' time =' + str(rtt[i]) + ' ms')
    return rtt


def reverse_ping(sock, args):
    rtt = {}
    global packets_dropped
    if not args.quiet:
        print('Operation : Reverse Ping')
        print('Server Address : ', IP_ADRRES)
        print('Client Address : ', IP_ADRRES)
    # enviar al server operacion y cantidad, y esperar respuesta.
    operacion = 'r-' + str(args.count or 5)
    if args.verbose:
        print('Inform server about reverse ping sending message: ', operacion)
    received = send_message(sock, operacion, args)
    if args.verbose:
        print('Received: ', received)
    if(received != b'start'):
        print('Server is busy.')
        return 1
    for i in range(args.count or 5):
        try:
            message, remote_address = sock.recvfrom(BUFFER_SIZE)
            print('Recieve: ' + six.ensure_str(message))
            sock.sendto(message, remote_address)
            print('Send: ' + six.ensure_str(message))
        except ConnectionRefusedError:
            if not args.quiet:
                print('ERROR: Connection refused due to wrong adress')
            return 'ERROR: Conection Refused'
        except Exception as e:
            if not args.quiet:
                print('ERROR: ', e)
            return 'ERROR: ' + e
    rtt_str, remote_address = sock.recvfrom(BUFFER_SIZE)
    rtt = literal_eval(six.ensure_str(rtt_str).split('-')[0])
    packets_dropped = int(literal_eval(six.ensure_str(rtt_str).split('-')[1]))
    print(rtt)
    print('packets_dropped {}'.format(packets_dropped))
    return rtt


def proxy_ping(sock, args):
    rtt = {}
    global packets_dropped
    if not args.quiet:
        print('Operation : Proxy Ping')
        print('Server Address : ', IP_ADRRES, ':', args.server[1])
        print('Detination Server Address : ', args.dest)
        print('Client Address : ', IP_ADRRES)
    # enviar al server operacion y cantidad, y esperar respuesta.
    operacion = 'x-' + str(args.count or 5) + '-{}'.format(args.dest)
    if args.verbose:
        print('Inform server about proxy ping sending message: ', operacion)
    received = send_message(sock, operacion, args)
    if args.verbose:
        print('Received: ', received)
    if(received != b'start'):
        print('Server is busy.')
        return 1
    rtt_str, _remote_address = sock.recvfrom(BUFFER_SIZE)
    rtt = literal_eval(six.ensure_str(rtt_str).split('-')[0])
    for k in six.iterkeys(rtt):
        print(IP_ADRRES, ':', args.server[1], ' received 6 bytes from '
              + str(args.dest) + ' seq =' + str(k) + ' time ='
              + str(rtt[str(k)]) + ' ms')
    packets_dropped = int(literal_eval(six.ensure_str(rtt_str).split('-')[1]))
    print(rtt)
    return rtt


def main():
    absolut_init_time = get_time()
    args = parse_args()
    if args.verbose:
        print('Server ip: ', args.server.split(':')[0])
        print('Server port: ', args.server.split(':')[1])
    server_address = (args.server.split(':')[0],
                      int(args.server.split(':')[1]))
    args.count = args.count if args.count else 5
    # Create socket and connect to server
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.settimeout(1)

    args.server = server_address
    rtt = {}
    if args.reverse:
        rtt = reverse_ping(sock, args)
    elif args.proxy:
        rtt = proxy_ping(sock, args)
    else:
        rtt = direct_ping(sock, args)
    if rtt:
        packets_transmitted = args.count
        packets_received = (args.count-packets_dropped)
        packets_loss = (packets_dropped/packets_transmitted) * 100
        rtt_min = round(min(six.itervalues(rtt)), 3)
        rtt_avg = round(sum(six.itervalues(rtt)) / len(rtt), 3)
        rtt_max = round(max(six.itervalues(rtt)), 3)
        rtt_mdev = round(sum([abs(rtt_avg-x) for x in six.itervalues(rtt)])
                         / len(rtt), 3)
        total_time = round(get_time() - absolut_init_time, 2)
        if args.verbose:
            print('--- {} ping statistics ---'.format(IP_ADRRES))
            print('{} packets transmitted, {} received, {}% packet loss,'
                  .format(
                      packets_transmitted,
                      packets_received, packets_loss)
                  + 'time %s ms' % total_time)
        if not args.quiet:
            print('rtt min/avg/max/mdev = {}/{}/{}/{} ms'
                  .format(rtt_min, rtt_avg, rtt_max, rtt_mdev))

    else:
        print('ERROR calculating statistics')

    sock.close()
    if args.verbose:
        print('Connection finished')
    return 'fin cliente'


if __name__ == "__main__":
    main()
