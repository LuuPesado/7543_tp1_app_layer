# 7543 TP1: App layer

Trabajo Práctico para la materia Introducción a los Sistemas Distribuidos (75.43) de la Facultad de Ingeniería de la UBA realizado por Alfreo Ackerman, Christian Angelone y Lucía Pesado. Este tp cumple con la estructura pedida en la consigna.

## Instalación Local

 ### Requisitos
  - Python >= 3.6.X (recomendamos utilizar [virtualenv](https://medium.com/@aaditya.chhabra/virtualenv-with-virtualenvwrapper-on-ubuntu-34850ab9e765))

 ### Python

 Para instalar el código y sus dependencias

 ```
 pip install -r requirements.txt
 or  
 pip3 install -r requirements.txt
 ```
## Comandos
 ```
usage: Client Process. [-h] [-v | -q] -s ADDR [-c COUNT] [-f] [-p | -r | -x] [-d ADDR]

Arguments:
  -h, --help            show this help message and exit
  -v, --verbose         increase output verbosity
  -q, --quiet           decrease output verbosity
  -s ADDR, --server ADDR
                        server IP address
  -c COUNT, --count COUNT
                        stop after < count > replies
  -f, --force           force random package loss
  -p, --ping            direct ping
  -r, --reverse         reverse ping
  -x, --proxy           proxy pin
  -d ADDR, --dest ADDR  destination IP address
 ```

## Ejecucion
**Servidor**

 ```
python3 tp_ping_srv.py -p 3000
 ```
Nosotros elegimos el puerto 3000, pero puede ser reemplazado por el que uno decida

**Cliente**


Para ejecutar un cliente es necesario especificar la dirección del servidor como se muestra a continuación.
 ```
python3 tp_ping.py -s 127.0.0.1:3000 
 ```
Si se ejecuta tal cual está en el ejemplo de arriba, se obtendrá por default un ping directo de 5 paquetes.

Con el comando -c se puede especificar la cantidad de paquetes
 ```
python3 tp_ping.py -s 127.0.0.1:3000 -c 10
 ```
 Se puede elegir entre las siguientes operaciones:
 ```
python3 tp_ping.py -s 127.0.0.1:3000 -p #ping directo
python3 tp_ping.py -s 127.0.0.1:3000 -r #ping reverso
python3 tp_ping.py -s 127.0.0.1:3000 -x -d 127.0.0.1:3001 #ping proxy
 ```
Tener en cuenta que el ping proxy requiere tener un server extra en el puerto 3001 (o el que se elija)

Si se quiere probar lo que pasaría si se pierde un paqute, se puede usar la opción -f (solo en la opción de ping directo)
 ```
python3 tp_ping.py -s 127.0.0.1:3000 -p -f
 ```
Además los comandos -v y -q aumentan y disminuyen la cantidad de información que se imprime en pantalla, segun se prefiera.

## CICD

Para asegurate que no vaya a fallar el pipeline, deberas correr los tests y flask8 por consola antes de commitear

```
flake8
```



